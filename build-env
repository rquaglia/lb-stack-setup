#!/bin/bash
set -eo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
BINARY_TAG="$( "$DIR/config.py" binaryTag )"
USE_DOCKER="$( "$DIR/config.py" useDocker )"

if [ "$USE_DOCKER" != true ]; then
    HOST_OS=$(/cvmfs/lhcb.cern.ch/lib/bin/host_os)
    if [ "$USE_DOCKER" = false ]; then
        if [ "$HOST_OS" != x86_64-centos7 ]; then
            echo "Host OS $HOST_OS is not supported. Find a CentOS 7 machine\n" \
                 "or use docker by setting useDocker = true in ${DIR}/config." >&2
            exit 1
        fi
    # else  # USE_DOCKER is not set in config
    #     # TODO check and propose?
    fi
fi

# TODO make kerberos optional
# TODO is there a way to forward cache from types other than FILE, e.g KCM?
# TODO if USE_DOCKER is false, any cache type is ok (no need to forward)
export KRB5CCNAME=$(klist 2>/dev/null | head -1 | grep -o 'FILE:.*')
if [ -z "$KRB5CCNAME" ]; then
    echo -e \
"No kerberos ticket cache of the form 'FILE:path/to/file' was found.
Please do this to forward a kerberos ticket to the conainer:

    export KRB5CCNAME='FILE:/tmp/krb5cc_$(id -u)'; kinit <username>@CERN.CH
"
    exit 1
fi

if [ "$USE_DOCKER" = true ]; then
    "${DIR}/lb-docker-run" \
        --quiet-env --lbenv -c ${BINARY_TAG} \
        -C "${DIR}/.." --use-absolute-path --kerberos \
        ${LB_DOCKER_RUN_FLAGS} \
        "$@"
else
    # TODO start a clean LbEnv a la lb-docker-run
    #      eg /cvmfs/lhcb.cern.ch/lib/LbEnv -c ${BINARY_TAG} ...
    export BINARY_TAG
    export CMTCONFIG="${BINARY_TAG}"
    "$@"
fi
